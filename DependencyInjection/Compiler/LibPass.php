<?php
/**
 * Created by Rubikin Team.
 * Date: 10/23/13
 * Time: 12:52 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\AsseticLibBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\Finder\Finder;

class LibPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if ($container->hasParameter('nilead.assetic.libs')) {
            $libs = $container->hasParameter('nilead.assetic.libs');
        }

        $finder = new Finder();
        $finder->files()->in(__DIR__ . '/../../Resources/config/libs')->name('*.yml');

        foreach ($finder as $file) {
            $libs[$file->getBaseName('.yml')] = array(
                'bundle' => 'NileadAsseticLibBundle',
                'path'   => 'libs',
                'file'   => $file->getRealpath()
            );
        }

        $container->setParameter('nilead.assetic.libs', $libs);
    }
}