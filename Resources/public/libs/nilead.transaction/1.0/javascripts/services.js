angular.module('nilead.module.transaction')
    .provider('Transaction', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
//                    list: function (orderId) {
//                        return Resource.get(Routing.generate('en__RG__nilead.backend.shipment.list', {orderId: orderId})).then(function(response) {
//                            return response.data;
//                        });
//                    },
                doCapture: function (transactionId) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.transaction.capture', {id: transactionId})).then(function (response) {
                        return response.data;
                    });
                },
                doVoid: function (transactionId) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.transaction.void', {id: transactionId})).then(function (response) {
                        return response.data;
                    });
                }
            }
        }]
    });