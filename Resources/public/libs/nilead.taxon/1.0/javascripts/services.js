angular.module('nilead.module.taxon')
    .provider('Taxon', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('nilead.taxon.backend.taxon.list', params)).then(function (response) {
                        return response.data;
                    });
                },

                show: function (id) {
                    return Resource.get(Routing.generate('nilead.taxon.backend.taxon.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },

                create: function (taxon, success, error) {
                    return Resource.post(Routing.generate('nilead.taxon.backend.taxon.create'), taxon, success, error).then(function (response) {
                        return response.data;
                    });
                },

                update: function (taxon, success, error) {
                    return Resource.put(Routing.generate('nilead.taxon.backend.taxon.update', {id: taxon.id}), taxon, success, error).then(function (response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('nilead.taxon.backend.taxon.delete', {id: id}))
                },

                getType: function () {
                    return Resource.get(Routing.generate('nilead.taxon.backend.taxon.get_type')).then(function (response) {
                        return response.data;
                    });
                },
                getParent: function () {
                    return Resource.get(Routing.generate('nilead.taxon.backend.taxon.get_parent')).then(function (response) {
                        return response.data;
                    });
                }
            }
        }]
    });
