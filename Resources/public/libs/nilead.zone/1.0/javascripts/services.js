angular.module('nilead.module.zone')
    .provider('Zone', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.zone.list', params)).then(function (response) {
                        return response.data;
                    });
                },
                createNew: function (zone, success, error) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.zone.create'), zone, success, error).then(function (response) {
                        return response.data;
                    });
                },
                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.zone.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },
                update: function (zone, success, error) {
                    return Resource.put(Routing.generate('en__RG__nilead.backend.zone.update', {id: zone.id}), zone, success, error).then(function (response) {
                        return response.data;
                    });
                },
                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.zone.delete', {id: id}))
                },
                getGeos: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.zone.get_geo', params)).then(function (response) {
                        return response.data;
                    });
                }
            }
        }]
    });