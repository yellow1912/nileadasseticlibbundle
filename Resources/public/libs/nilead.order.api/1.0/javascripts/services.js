angular.module('nilead.module.order.api')
    .provider('Order', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function () {
                    return Resource.get(Routing.generate('en__RG__nilead.api.order.list')).then(function (response) {
                        return undefined !== response.data.orders ? response.data.orders : {};
                    });
                },
                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.api.order.show', {id: id})).then(function (response) {
                        return undefined !== response.data.order ? response.data.order : {};
                    });
                }
            }
        }];
    })
;