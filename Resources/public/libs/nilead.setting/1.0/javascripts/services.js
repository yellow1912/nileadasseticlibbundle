angular.module('nilead.module.setting')
    .provider('Setting', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                get: function (unique) {
                    return Resource.get(Routing.generate('nilead.setting.backend.setting.get', {namespace: unique})).then(function (response) {
                        return response.data;
                    });
                },
                update: function (namespace, setting, success, error) {
                    return Resource.put(Routing.generate('nilead.setting.backend.setting.update', {namespace: namespace}), setting, success, error).then(function (response) {
                        return response.data;
                    });
                }
            }
        }]
    });
