/**
 * jQuery Nilead Imagine v2.0.1
 *
 * Created by Rubikin Team.
 * ========================
 * Date: 2014-04-24
 * Time: 12:20:09 AM
 *
 * Question? Come to our website at http://rubikin.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

;(function ($, window, document, undefined) {
    // Create the defaults once
    var pluginName = 'nileadImagine';
    var defaults = {
        skipError: false,
        forceUseStyler: false,
        position: 50,

        resizeTimeout: 500,

        stylerTemplate: '<span></span>',
        stylerClass: 'nl-imagine-styler',

        wrapperTemplate: '<span></span>',
        wrapperClass: 'nl-imagine-wrapper',

        itemResolver: null,

        // Map
        autoScaleMap: true,

        // Highlighter
        highlightEnabled: true,

        // For more informations about these settings,
        // Please visit: http://www.w3schools.com/tags/ref_canvas.asp
        fillStyle: '#000000',
        font: '10px sans-serif',
        globalAlpha: 1,
        globalCompositeOperation: 'source-over',
        imageSmoothingEnabled: true,
        lineCap: 'butt',
        lineDashOffset: 0,
        lineJoin: 'miter',
        lineWidth: 1,
        miterLimit: 10,
        shadowBlur: 0,
        shadowColor: 'rgba(0, 0, 0, 0)',
        shadowOffsetX: 0,
        shadowOffsetY: 0,
        strokeStyle: '#ff0000',
        textAlign: 'start',
        textBaseline: 'alphabetic',
        webkitBackingStorePixelRatio: 1,
        webkitImageSmoothingEnabled: true
    };

    // The constructor
    function Plugin (element, options) {
        this.element = element;
        this.$element = $(element);

        this.options = $.extend({}, defaults, options) ;

        this._defaults = defaults;
        this._name = pluginName;

        this.attachEvent();
        this.process();

        var self = this;

        this.$element.load(function (event) {
            self.process();
        });
    }

    // The prototype
    Plugin.prototype = {
        attachEvent: function () {
            $(window).resize({ self: this}, function (event) {
                var self = event.data.self;

                if (self.resizeTimeout) {
                    clearTimeout(self.resizeTimeout);
                }

                self.resizeTimeout = setTimeout(function () {
                    self.process();
                }, self.options.resizeTimeout);
            });
        },

        process: function () {
            // Detect current element is a HTMLImageElement instance
            // If not, and we have an itemResolver, try to resole the HTMLImageElement
            if (false === this.isImageElement()) {
                if ( ! this.options.skipError) {
                    throw new Error('Expected argument type of "IMG", "' + this.element.tagName + '" given');
                }

                return;
            }

            // If this element is a nilead imagine (processed)
            // Just restore it to original
            if (true === this.isNileadImagine()) {
                this.restore();
            }

            // If the naturalWidth property is not defined (IE8 or lower)
            // This mean the naturalHeight property also undefined
            // We must call the function to fix it
            if (undefined === this.element.naturalWidth) {
                this.fixNaturalSizes();
            }

            // Calculate horizontal and vertical ratio between display & original size
            var horizontalRatio = (this.$element.width() / this.element.naturalWidth).toFixed(3);
            var verticalRatio = (this.$element.height() / this.element.naturalHeight).toFixed(3);

            // If the horizontalRatio do not equal to verticalRatio
            // The element need to scale
            if (horizontalRatio !== verticalRatio && (horizontalRatio < 1 || verticalRatio < 1)) {
                this.wrap();

                // If the horizontal ratio larger than vertical ratio
                // We need to scale image by width
                if (horizontalRatio > verticalRatio) {
                    this.scaleImageByWidth(horizontalRatio);
                }

                // Otherwise, scale image by height
                else {
                    this.scaleImageByHeight(verticalRatio);
                }

                this.element.isNileadImagine = true;
            } else {
                this.element.isNileadImagine = false;
            }

            if (this.hasMap()) {
                this.map = $('map[name="' + this.$element.attr('usemap').replace('#', '') + '"]');

                if (true === this.options.autoScaleMap && this.isScaledElement()) {
                    this.scaleMap();
                }

                if (true === this.options.highlightEnabled) {
                    this.highLighting();
                }
            }
        },

        scaleMap: function () {
            var self = this;

            this.map.find('area').each(function (index) {
                self.scaleArea(this);
            });
        },

        scaleArea: function (element) {
            var $element = $(element);
            var horizontalRatio = this.$element.width() / this.element.naturalWidth;
            var verticalRatio = this.$element.height() / this.element.naturalHeight;

            if (undefined === element.originalCoords) {
                element.originalCoords = $element.attr('coords');
            }

            var coords = element.originalCoords.split(',');
            var newCoords = [];
            var marginTop = parseInt(this.element.style.marginTop) || 0;
            var marginLeft = parseInt(this.element.style.marginLeft) || 0;

            for (var i = 0; i < coords.length; i++) {
                coord = parseFloat(coords[i]);

                newCoords.push(coord * (0 === i % 2 ? horizontalRatio : verticalRatio));
            }

            $element.attr('coords', newCoords.join(','));
        },

        highLighting: function () {
            var relative = $('<div style="position: relative;"></div>');
            var canvas = $('<canvas></canvas>');

            canvas.attr('width', parseInt(this.$element.width()));
            canvas.attr('height', parseInt(this.$element.height()));
            canvas.css({
                // display: 'none',
                zIndex: 1,
                marginTop: parseInt(this.element.style.marginTop) || 0,
                marginLeft: parseInt(this.element.style.marginLeft) || 0
            });

            var context = canvas[0].getContext('2d');

            this.$element.before(relative).prev().append(this.element);

            this.$element.after(this.$element.clone()).after(canvas).css({
                opacity: 0,
                zIndex: 9999
            });

            this.map.find('area').each(function (index) {
                $(this).mouseenter(function (event) {
                    var coords = event.target.coords.toString().split(',');
                    context.beginPath();

                    for (var i = 0; i < coords.length; i += 2) {
                        var x = parseInt(coords[i]);
                        var y = parseInt(coords[i + 1]);

                        if (0 !== i) {
                            context.lineTo(x, y);

                        } else {
                            context.moveTo(x, y);
                        }
                    }

                    context.closePath();

                    context.lineWidth = 1;
                    context.fillStyle = '#FAD183';
                    context.shadowColor = '#FAF15C';
                    context.shadowBlur = 5;
                    context.shadowOffsetX = 0;
                    context.shadowOffsetY = 0;
                    context.strokeStyle = '#FAF15C';

                    context.fill();
                    context.stroke();

                    // canvas.fadeIn();
                });
            }).mouseleave(function (event) {
                // canvas.fadeOut(function () {
                    context.clearRect(0, 0, canvas.attr('width'), canvas.attr('height'));
                // });
            });;
        },

        /**
         * Detect the given element is an HTMLImageElement instance
         *
         * @return boolean
         */
        isImageElement: function () {
            if (this.element instanceof HTMLImageElement) {
                return true;
            } else if ('function' === typeof this.options.itemResolver) {
                // If we have a resolver, try to resolve the image element
                this.element = this.options.itemResolver(this.element);

                // Check again after resolved new image element
                if (this.element instanceof HTMLImageElement) {
                    this.$element = $(this.element);

                    return true;
                }
            }

            return false;
        },

        /**
         * Is the given element processed?
         *
         * @return boolean
         */
        isNileadImagine: function () {
            return true === this.element.isNileadImagine;
        },

        /**
         * Is this element has any padding property?
         *
         * @return boolean
         */
        isPaddingElement: function () {
            return this.$element.width() !== this.$element.innerWidth() || this.$element.height() !== this.$element.innerHeight();
        },

        /**
         * Is this element scaled?
         *
         * @return boolean
         */
        isScaledElement: function () {
            return this.$element.width() !== this.element.naturalWidth || this.$element.height() !== this.element.naturalHeight;
        },

        hasMap: function () {
            return undefined !== this.$element.attr('usemap');
        },

        /**
         * Fix image natural sizes (for IE8 or lower)
         *
         * @return void
         */
        fixNaturalSizes: function () {
            var ghostImage = new Image();

            ghostImage.src = this.element.src;

            this.element.naturalWidth = ghostImage.width;
            this.element.naturalHeight = ghostImage.height;
        },

        mergeStyle: function (target) {
            var sourceStyles = this.$element.attr('style') || '';
            var targetStyles = target.attr('style') || '';

            sourceStyles = sourceStyles.split(';');
            targetStyles = targetStyles.split(';');

            var styles = [];

            for (var i = 0, style; style = sourceStyles[i]; i++) {
                style = style.split(':');
                styles[$.trim(style[0])] = $.trim(style[1]);
            }

            for (var i = 0, style; style = targetStyles[i]; i++) {
                style = style.split(':');
                styles[$.trim(style[0])] = $.trim(style[1]);
            }

            target.css(styles);
        },

        wrap: function () {
            this.wrapper = $(this.options.wrapperTemplate);

            this.$element
                .before(this.wrapper)
                .prev()
                .append(this.element)
                .width(this.$element.width())
                .height(this.$element.height())
            ;

            // If the wrapperClass option is configured
            // Add this class into the wrapper
            // IMPORTANT: in the wrapperClass, must define some required properties below
            if (this.options.wrapperClass) {
                this.wrapper.addClass(this.options.wrapperClass);
            }

            // Otherwise, apply some required properties
            else {
                this.wrapper.css({
                    display: 'inline-block',
                    overflow: 'hidden',
                    verticalAlign: 'bottom'
                });
            }

            if (this.isPaddingElement() || true === this.options.forceUseStyler) {
                this.wrapStyle();
            } else {
                this.wrapper
                    .addClass(this.element.className)
                    .attr('style', this.mergeStyle(this.wrapper))
                ;
            }

            this.$element
                .removeAttr('class')
                .removeAttr('style')
            ;
        },

        wrapStyle: function () {
            this.styler = $(this.options.stylerTemplate);

            this.wrapper
                .before(this.styler)
                .prev()
                .append(this.wrapper)
                .addClass(this.element.className)
                .attr('style', this.mergeStyle(this.styler))
            ;

            // If the stylerClass setting is configured
            // Add this class into the styler
            // IMPORTANT: in the stylerClass, must define some required properties below
            if (this.options.stylerClass) {
                this.styler.addClass(this.options.stylerClass);
            }

            // Otherwise, apply some required properties
            else {
                this.styler.css({
                    display: 'inline-block',
                    lineHeight: 0,
                    verticalAlign: 'bottom'
                });
            }
        },

        /**
         * Scale image by horizontal direction
         *
         * @param  float ratio
         *
         * @return void
         */
        scaleImageByWidth: function (ratio) {
            var diffSize = Math.abs(Math.floor(this.element.naturalHeight * ratio - this.wrapper.height()));

            this.element.style.maxWidth = this.wrapper.width() + 'px';
            this.element.style.marginTop = this.getElementPosition(diffSize) + 'px';
        },

        /**
         * Scale image by vertical direction
         *
         * @param  float ratio
         *
         * @return void
         */
        scaleImageByHeight: function (ratio) {
            var diffSize = Math.abs(Math.floor(this.element.naturalWidth * ratio - this.wrapper.width()));

            this.element.style.maxHeight = this.wrapper.height() + 'px';
            this.element.style.marginLeft = this.getElementPosition(diffSize) + 'px';
        },

        /**
         * Calculate image position after scaled by pixels or percentage
         *
         * @param  integer diffSize
         *
         * @return integer
         */
        getElementPosition: function (diffSize) {
            // Get position from element data-postion attribute
            var position = this.options.position;

            if ('string' === typeof position && 0 < position.search('px')) {
                // Calculate position as pixels
                position = parseInt(position);
            } else {
                // Calculate position as percentage
                position = Math.floor(parseInt(position) * diffSize / 100);
            }

            return position < 0 ? 0 : position > diffSize ? -diffSize : -position;
        },

        /**
         * Restore the processed HTMLImageElement to original
         *
         * @return void
         */
        restore: function () {
            var classes = '';
            this.$element.removeAttr('style');

            if (this.wrapper) {
                classes = this.wrapper.attr('class');
            }

            if (this.styler) {
                classes += ' ' + this.styler.attr('class');
            }

            classes = classes.split(' ');
            for (var i = 0, curClass; curClass = classes[i]; i++) {
                curClass = $.trim(curClass);

                if (curClass !== this.options.wrapperClass && curClass !== this.options.stylerClass) {
                    this.$element.addClass(curClass);
                }
            }

            if (this.styler) {
                this.styler.after(this.$element);

                this.styler.remove();
            } else if (this.wrapper) {
                this.wrapper.after(this.$element);

                this.wrapper.remove();
            }
        }
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if ( ! $.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    };
})(jQuery, window, document);
