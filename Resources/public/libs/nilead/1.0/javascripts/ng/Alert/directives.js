angular.module('nilead.module.alert')
    .directive('nlAlert', ['Alert', function (Alert) {
        return {
            templateUrl: 'alert.html',
            link: function (scope, elem, attrs) {
                scope.alerts = Alert.get();

                scope.$watchCollection('alerts', function () {
                    if (!_.isEmpty(scope.alerts)) {
                        elem[0].focus();
                    }
                })
            }
        };
    }]);