var ng = {
    deps: {},
    dependsOn: function ($ns, $dep) {

        if (_.isString($dep)) {
            $dep = [$dep];
        }

        if (undefined === this.deps[$ns]) {
            this.deps[$ns] = [];
        }

        this.deps[$ns] = _.union(this.deps[$ns], $dep);
    },
    getDependencies: function ($ns) {
        if (undefined === this.deps[$ns]) {
            return [];
        } else {
            return this.deps[$ns]
        }
    }
}

var nl = {
    utility:
        {
        path: function (obj, path, def) {

            for (var i = 0, path = path.split('.'), len = path.length; i < len; i++) {
                if (!obj || typeof obj !== 'object') return def;
                obj = obj[path[i]];
            }

            if (obj === undefined) return def;
            return obj;
        }
    }
}