angular.module('nilead.module.resource')
    .provider('Resource', function () {
        this.$get = ['responseProcessor', 'Restangular', function (responseProcessor, Restangular) {

            var helper = function () {
                return {
                    success: function (response, callback) {
                        if ('function' === typeof callback) {
                            callback(response);
                        }

                        responseProcessor.processSuccess(response);

                        return response;
                    },

                    error: function (response, callback) {
                        if ('function' === typeof callback) {
                            callback(response);
                        }

                        responseProcessor.processError(response);

                        return response;
                    }
                }
            }();

            return {
                get: function (url, success, error) {
                    return Restangular.oneUrl('url', url).get().then(function (response) {
                        return helper.success(response, success);
                    }, function (response) {
                        return helper.error(response, error);
                    });
                },

                post: function (url, data, success, error) {
                    if (data instanceof FormData) {
                        return Restangular.withConfig(function (RestangularConfigurer) {
                                RestangularConfigurer.setDefaultHeaders({'Content-Type': undefined});
                            })
                            .oneUrl('url', url)
                            .withHttpConfig({transformRequest: angular.identity})
                            .customPOST(data)
                            .then(function (response) {
                                return helper.success(response, success);
                            }, function (response) {
                                return helper.error(response, error);
                            })
                        ;
                    } else {
                        return Restangular.oneUrl('url', url).post('', data)
                            .then(function (response) {
                                return helper.success(response, success);
                            }, function (response) {
                                return helper.error(response, error);
                            })
                        ;
                    }
                },

                put: function (url, data, success, error) {
                    return Restangular.oneUrl('url', url).customPUT(data)
                        .then(function (response) {
                            return helper.success(response, success);
                        }, function (response) {
                            return helper.error(response, error);
                        })
                    ;
                },

                remove: function (url, data, success, error) {
                    return Restangular.oneUrl('url', url).remove('', data)
                        .then(function (response) {
                            return helper.success(response, success);
                        }, function (response) {
                            return helper.error(response, error);
                        })
                    ;
                }
            }
        }]
    });
