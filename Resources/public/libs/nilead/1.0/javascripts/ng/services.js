angular.module('nilead.services', ['nilead.module.resource', 'nilead.module.response_processor'])
    .factory('cartFactory', ['$rootScope', 'responseProcessor', 'Resource', function ($rootScope, responseProcessor, Resource) {
        var promise = false;
        var cart = {};

        return {
            set: function ($cart) {
                cart = $cart;

                $rootScope.$broadcast('NILEAD_CART_UPDATE', cart);
            }, get: function () {
                return cart;
            }, load: function () {
                var _this = this;
                if (! promise) {
                    promise = Resource.get(Routing.generate('en__RG__nilead.api.cart.show')).then(function (response) {

                        _this.set(undefined !== response.data.cart ? response.data.cart : {});

                        return _this.get();
                    });
                }

                return promise;
            }, removeItem: function (itemId) {
                var _this = this;
                promise = Resource.post(Routing.generate('en__RG__nilead.frontend.cart.remove'), {id: itemId}).then(function (response) {

                    _this.set(undefined !== response.data.cart ? response.data.cart : {});

                    return _this.get();
                });

                return promise;
            }
        }
    }])
    .provider('SimpleData', function () {
        this.$get = [function () {
            var data = {};

            return {
                set: function (key, value) {
                    data[key] = value;
                }, get: function (key, defaultValue) {
                    return data.hasOwnProperty(key) ? data[key] : defaultValue;
                }, has: function (key) {
                    return data.hasOwnProperty(key);
                }, reset: function () {
                    data = {};
                }
            }
        }]
    });
