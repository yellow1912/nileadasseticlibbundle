angular.module('nilead.module.response_processor')
    .provider('responseProcessor', function () {

        var processMessages = function (Alert, response) {
            Alert.reset();

            /**
             * errors{
             *     'field' => [
             *         'error 1',
             *         'error 2'
             *     ]
             * }
             */
            if (angular.isDefined(response.errors)) {
                angular.forEach(response.errors, function (fieldName, fieldValue) {
                    angular.forEach(fieldValue, function (messageValue, messageKey) {
                        Alert.add('errors', fieldName, messageValue);
                    });
                });
            }

            /**
             * messages[
             *     'message 1',
             *     'message 2'
             * ]
             */
            if (angular.isDefined(response.messages)) {
                angular.forEach(response.messages, function (messageValue, messageKey) {
                    Alert.add('global', 'messages', messageValue);
                });
            }

            if (angular.isDefined(response.message)) {
                Alert.add('global', 'messages', response.message);
            }
        }

        this.$get = ['$window', 'Alert', function ($window, Alert) {
            return {
                processSuccess: function (response) {
                    if (typeof response !== "undefined") {
                        if (undefined !== response.redirect) { // redirect
                            $window.location.href = response.redirect;
                        }

                        processMessages(Alert, response);
                    }
                },
                processError: function (response) {
                    if (typeof response !== "undefined") {
                        processMessages(Alert, response.data);

                        if (undefined !== response.data.redirect) { // redirect
                            $window.location.href = response.data.redirect;
                        }
                    }
                },
                getAlert: function () {
                    return Alert;
                }
            }
        }]
    })