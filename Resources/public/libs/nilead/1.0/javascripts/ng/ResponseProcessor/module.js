angular.module('nilead.module.response_processor', ['nilead.module.alert'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    });