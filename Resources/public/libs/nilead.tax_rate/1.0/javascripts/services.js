angular.module('nilead.module.tax_rate')
    .provider('TaxRate', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.tax_rate.list', params)).then(function (response) {
                        return response.data;
                    });
                },

                createNew: function (taxRate, success, error) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.tax_rate.create'), taxRate, success, error).then(function (response) {
                        return response.data;
                    });
                },

                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.tax_rate.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },

                update: function (taxRate, success, error) {
                    return Resource.put(Routing.generate('en__RG__nilead.backend.tax_rate.update', {id: taxRate.id}), taxRate, success, error).then(function (response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.tax_rate.delete', {id: id}))
                }
            }
        }]
    });