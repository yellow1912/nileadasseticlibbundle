angular.module('nilead.module.discount')
    .provider('Discount', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.discount.list', params)).then(function (response) {
                        return response.data;
                    });
                },

                createNew: function (discount, success, error) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.discount.create'), discount, success, error).then(function (response) {
                        return response.data;
                    });
                },

                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.discount.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },

                update: function (discount, success, error) {
                    return Resource.put(Routing.generate('en__RG__nilead.backend.discount.update', {id: discount.id}), discount, success, error).then(function (response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.discount.delete', {id: id}))
                }
            }
        }]
    });