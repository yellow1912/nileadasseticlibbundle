angular.module('nilead.module.currency')
    .provider('Currency', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {

                list: function (params) {
                    return Resource.get(Routing.generate('nilead.pricing.backend.currency.list', params)).then(function (response) {
                        return response.data;
                    });
                },

                create: function (currency, success, error) {
                    return Resource.post(Routing.generate('nilead.pricing.backend.currency.create'), currency, success, error).then(function (response) {
                        return response.data;
                    });
                },

                show: function (id) {
                    return Resource.get(Routing.generate('nilead.pricing.backend.currency.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },

                update: function (currency, success, error) {
                    return Resource.put(Routing.generate('nilead.pricing.backend.currency.update', {id: currency.id}), currency, success, error).then(function (response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('nilead.pricing.backend.currency.delete', {id: id}))
                }
            }
        }]
    });
