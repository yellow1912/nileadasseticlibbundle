angular.module('nilead.module.page')
    .provider('Page', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.page.list', params)).then(function (response) {
                        return response.data;
                    });
                },

                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.page.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },

                getExtraData: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.page.get_extras_data', {id: id})).then(function (response) {
                        return response.data;
                    });
                },

                createNew: function (page, success, error) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.page.create'), page, success, error).then(function (response) {
                        return response.data;
                    });
                },

                update: function (page, success, error) {
                    return Resource.put(Routing.generate('en__RG__nilead.backend.page.update', {id: page.id}), page, success, error).then(function (response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.page.delete', {id: id}))
                },

                getTemplates: function () {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.page.get_templates')).then(function (response) {
                        return response.data;
                    });
                }
            }
        }]
    });