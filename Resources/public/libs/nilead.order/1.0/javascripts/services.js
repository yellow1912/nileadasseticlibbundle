angular.module('nilead.module.order')
    .provider('Order', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.order.list', params)).then(function (response) {
                        return response.data;
                    });
                },
                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.order.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },
                invoice: {
                    void: function (orderId, invoiceId) {
                        return Resource.post(Routing.generate('en__RG__nilead.backend.invoice.void', {orderId: orderId, invoiceId: invoiceId})).then(function (response) {
                            return response.data;
                        });
                    },
                    list: function (orderId) {
                        return Resource.get(Routing.generate('en__RG__nilead.backend.invoice.list', {orderId: orderId})).then(function (response) {
                            return response.data;
                        });
                    }
                },
                shipment: {
                    list: function (orderId) {
                        return Resource.get(Routing.generate('en__RG__nilead.backend.shipment.list', {orderId: orderId})).then(function (response) {
                            return response.data;
                        });
                    },
                    complete: function (orderId, shipmentId) {
                        return Resource.post(Routing.generate('en__RG__nilead.backend.shipment.complete', {orderId: orderId, shipmentId: shipmentId})).then(function (response) {
                            return response.data;
                        });
                    },
                    ship: function (orderId, shipmentId, trackingId) {
                        return Resource.post(Routing.generate('en__RG__nilead.backend.shipment.ship', {orderId: orderId, shipmentId: shipmentId, trackingId: trackingId})).then(function (response) {
                            return response.data;
                        });
                    },
                    cancel: function (orderId, shipmentId) {
                        return Resource.post(Routing.generate('en__RG__nilead.backend.shipment.cancel', {orderId: orderId, shipmentId: shipmentId})).then(function (response) {
                            return response.data;
                        });
                    }
                },
                transaction: {
//                    list: function (orderId) {
//                        return Resource.get(Routing.generate('en__RG__nilead.backend.shipment.list', {orderId: orderId})).then(function(response) {
//                            return response.data;
//                        });
//                    },
                    capture: function (paymendId) {
                        return Resource.post(Routing.generate('en__RG__nilead.backend.payment.capture', {id: paymendId})).then(function (response) {
                            return response.data;
                        });
                    },
                    void: function (paymendId) {
                        return Resource.post(Routing.generate('en__RG__nilead.backend.payment.void', {id: paymendId})).then(function (response) {
                            return response.data;
                        });
                    }
                }
            }
        }]
    });