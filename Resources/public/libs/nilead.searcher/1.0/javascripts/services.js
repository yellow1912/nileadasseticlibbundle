/**
 * Variable post affect method post in javascript , replace to blog
 */
angular.module('nilead.module.searcher').provider('Searcher', function () {

        this.$get = ['responseProcessor', 'Resource', '$rootScope', function (responseProcessor, Resource, $rootScope) {

            return {
                search: function (page, criteria, sorters, aggs, initialized) {

                    return Resource.get(Routing.generate('nilead.search.order', {
                        page: page,
                        criteria: criteria,
                        sorters: sorters,
                        aggs: aggs,
                        initialized: initialized
                    })).then(function (response) {
                        $rootScope.$broadcast('NILEAD_SEARCH_UPDATE', response.data);
                        return response.data;
                    });
                }
            }
        }]
    });
