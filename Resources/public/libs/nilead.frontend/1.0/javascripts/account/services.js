angular.module('nilead.frontend.account')
    .provider('Account', function () {
        this.$get = ['$rootScope', 'Resource', function ($rootScope, Resource) {
            return {
                login: function (loginInfo) {
                    return Resource.post(Routing.generate('en__RG__nilead.frontend.account.login_check'), loginInfo,
                        function () {
                            $rootScope.$broadcast('NILEAD_LOGIN_SUCCESS');
                        },
                        function () {
                            $rootScope.$broadcast('NILEAD_LOGIN_FAIL');
                        });
                },
                register: function (registrationInfo) {
                    return Resource.post(Routing.generate('en__RG__nilead.frontend.registration.register'), registrationInfo,
                        function () {
                            $rootScope.$broadcast('NILEAD_REGISTER_SUCCESS');
                        },
                        function () {
                            $rootScope.$broadcast('NILEAD_REGISTER_FAIL');
                        });
                }
            }
        }]
    });