/**
 * Variable post affect method post in javascript , replace to blog
 */
angular.module('nilead.module.post')
    .provider('Post', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {

                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.post.list', params)).then(function (response) {
                        return response.data;
                    });
                },

                createNew: function (blog, success, error) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.post.create'), blog, success, error).then(function (response) {
                        return undefined !== response.data ? response.data : {}
                    });
                },

                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.post.show', {id: id})).then(function (response) {
                        return undefined !== response.data ? response.data : {}
                    });
                },

                update: function (post, data) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.post.update', {id: post.id}), data).then(function (response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.post.delete', {id: id}));
                },

                getTaxons: function () {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.post.get_taxons')).then(function (response) {
                        return undefined !== response.data ? response.data : {}
                    });
                },

                getExtraData: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.post.get_extras_data', {id: id})).then(function (response) {
                        return undefined !== response.data ? response.data : {}
                    });
                },

                getTemplates: function () {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.post.get_templates')).then(function (response) {
                        return response.data;
                    });
                }
            }
        }]
    });