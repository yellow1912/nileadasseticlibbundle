angular.module('nilead.module.address')
    .directive('address', [function () {
        return {
            templateUrl: 'address.html',
            scope: {
                address: '='
            }
        };
    }]);