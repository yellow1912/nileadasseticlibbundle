angular.module('nilead.module.shipping_category')
    .provider('ShippingCategory', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.shipping_category.list', params)).then(function (response) {
                        return response.data;
                    });
                },

                createNew: function (shippingCategory, success, error) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.shipping_category.create'), shippingCategory, success, error).then(function (response) {
                        return response.data;
                    });
                },

                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.shipping_category.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },

                update: function (shippingCategory, success, error) {
                    return Resource.put(Routing.generate('en__RG__nilead.backend.shipping_category.update', {id: shippingCategory.id}), shippingCategory, success, error).then(function (response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.shipping_category.delete', {id: id}))
                }
            }
        }]
    });