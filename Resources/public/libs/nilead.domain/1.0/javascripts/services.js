angular.module('nilead.module.domain')
    .provider('Domain', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.domain.list', params));
                },

                createNew: function (domain) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.domain.create'), domain).then(function (response) {
                        return response.data;
                    });
                },

                setPrimary: function (id) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.domain.set_primary',{id: id})).then(function(response) {
                        return response.data;
                    });
                },

                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.domain.show', {id: id})).then(function(response) {
                        return response.data;
                    });
                },

                update: function(domain){
                    return Resource.put(Routing.generate('en__RG__nilead.backend.domain.update',{id: domain.id}), domain).then(function(response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.domain.delete', {id: id}))
                }
            }
        }]
    });